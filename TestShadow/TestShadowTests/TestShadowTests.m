//
//  TestShadowTests.m
//  TestShadowTests
//
//  Created by Andrei Stanescu on 7/18/13.
//  Copyright (c) 2013 Mind Treat Studios. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface TestShadowTests : XCTestCase

@end

@implementation TestShadowTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
