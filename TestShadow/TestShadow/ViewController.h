//
//  ViewController.h
//  TestShadow
//
//  Created by Andrei Stanescu on 7/18/13.
//  Copyright (c) 2013 Mind Treat Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomButton.h"

@interface ViewController : UIViewController

@end
