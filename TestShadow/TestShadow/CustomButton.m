//
//  CustomButton.m
//  TestShadow
//
//  Created by Andrei Stanescu on 7/18/13.
//  Copyright (c) 2013 Mind Treat Studios. All rights reserved.
//

#import "CustomButton.h"

@implementation CustomButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGColorRef colorRef = [[UIColor blueColor] CGColor];
    CGContextSetStrokeColorWithColor(context, colorRef);
    CGContextSetFillColorWithColor(context, [[UIColor blueColor] CGColor]);
    
    
    CGRect buttonRect = CGRectInset(rect, 3, 3);
    
    CGPoint centerPoint = CGPointMake(CGRectGetMidX(buttonRect  ),     CGRectGetMidY(buttonRect));
    CGFloat radius = CGRectGetWidth(buttonRect) / 2;
    
    CGContextSaveGState(context);
    CGContextSetShadow(context, CGSizeMake(2.0, 2.0), 2.0);
    CGContextAddArc(context, centerPoint.x, centerPoint.y, radius, 0, 2*M_PI, 1);
    //CGContextClip(context);
    CGContextFillPath(context);
    CGContextRestoreGState(context);
    
    
    CGColorRelease(colorRef);
}


@end
